package pl.sda.interfaces;

public interface Figure {
    double getArea();

    default double getPerimeter() {
        return 0;
    }

    default void print() {
        System.out.println(getArea());
        System.out.println(getPerimeter());
    }
}
