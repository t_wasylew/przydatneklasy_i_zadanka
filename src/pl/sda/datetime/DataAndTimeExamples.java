package pl.sda.datetime;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;

public class DataAndTimeExamples {
    public static void main(String[] args) {
        // #1
        date();

        // #2
        calendar();

        // #3
        localDate();

        // #4
        localTime();

        // #5
        localDateTime();

        // #6
        zoneDateTime();

        // #7
        period();

        // #8
        duration();
    }

    private static void date() {
        Date date = new Date();
        System.out.println("Date: " + date);
    }

    private static void calendar() {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Calendar: " + calendar);
    }

    private static void localDate() {
        LocalDate localDate = LocalDate.now();
        System.out.println("LocalDate: " + localDate);

        LocalDate localDateOf = LocalDate.of(2015, 2, 20);
        System.out.println("LocalDateOf: " + localDateOf);

        LocalDate localDateParse = LocalDate.parse("2015-02-20");
        System.out.println("LocalDateParse: " + localDateParse);

        LocalDate localDateTomorrow = LocalDate.now().plusDays(1);
        System.out.println("LocalDateTomorrow: " + localDateTomorrow);

        LocalDate previousMonthSameDay = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        System.out.println("LocalDatePreviousMonthSameDay: " + previousMonthSameDay);

        DayOfWeek localDateSunday = LocalDate.parse("2017-12-31").getDayOfWeek();
        System.out.println("LocalDateSunday: " + localDateSunday);

        int localDateFirst = LocalDate.parse("2017-12-01").getDayOfMonth();
        System.out.println("LocalDateFirst: " + localDateFirst);

        boolean localDateLeapYear = LocalDate.now().isLeapYear();
        System.out.println("LocalDateLeapYear: " + localDateLeapYear);

        boolean localDateNotBefore = LocalDate.parse("2016-06-12").isBefore(LocalDate.parse("2016-06-11"));
        System.out.println("LocalDateNotBefore: " + localDateNotBefore);

        boolean localDateIsAfter = LocalDate.parse("2016-06-12").isAfter(LocalDate.parse("2016-06-11"));
        System.out.println("LocalDateIsAfter: " + localDateIsAfter);

        LocalDate localDateFirstDayOfMonth = LocalDate.parse("2016-06-12").with(TemporalAdjusters.firstDayOfMonth());
        System.out.println("LocalDateFirstDayOfMonth: " + localDateFirstDayOfMonth);
    }

    private static void localTime() {
        LocalTime localTime = LocalTime.now();
        System.out.println("LocalTime: " + localTime);

        LocalTime localTimeNine = LocalTime.parse("09:00");
        System.out.println("LocalTimeNine: " + localTimeNine);

        LocalTime localTimeSixThirty = LocalTime.of(6, 30);
        System.out.println("LocalTimeSixThirty: " + localTimeSixThirty);

        LocalTime localTimeSevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);
        System.out.println("LocalTimeSevenThirty: " + localTimeSevenThirty);

        int localTimeSix = LocalTime.parse("06:30").getHour();
        System.out.println("LocalTimeSix: " + localTimeSix);

        boolean localTimeIsbefore = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));
        System.out.println("LocalTimeIsbefore: " + localTimeIsbefore);
    }

    private static void localDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("LocalDateTime: " + localDateTime);

        LocalDateTime localDateTimeOf = LocalDateTime.of(2017, Month.SEPTEMBER, 30, 6, 30);
        System.out.println("LocalDateTimeOf: " + localDateTimeOf);

        LocalDateTime localDateTimeParse = LocalDateTime.parse("2017-09-30T06:30:00");
        System.out.println("LocalDateTimeParse: " + localDateTimeParse);

        LocalDateTime localDateTimePlusDay = localDateTime.plusDays(1);
        System.out.println("LocalDateTimePlusDay: " + localDateTimePlusDay);

        LocalDateTime localDateTimeMinusTwoHours = localDateTime.minusHours(2);
        System.out.println("LocalDateTimeMinusTwoHours: " + localDateTimeMinusTwoHours);

        Month localDateTimeMonth = localDateTime.getMonth();
        System.out.println("LocalDateTimeMonth: " + localDateTimeMonth);

        LocalDateTime localDateTimeBeginningOfDay = LocalDate.parse("2016-06-12").atStartOfDay();
        System.out.println("LocalDateTimeBeginningOfDay: " + localDateTimeBeginningOfDay);
    }

    private static void zoneDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();

        ZoneId zoneId = ZoneId.of("Europe/Warsaw");
        System.out.println("ZoneId: " + zoneId);

        System.out.println("All ZoneId: " + ZoneId.getAvailableZoneIds());

        System.out.println("ZonedDateTime: " + ZonedDateTime.of(LocalDateTime.now(), zoneId));
        System.out.println("ZonedDateTimeParse: " + ZonedDateTime.parse("2017-09-30T10:15:30+02:00[Europe/Warsaw]"));

        ZoneOffset zoneOffset = ZoneOffset.of("+02:00");
        System.out.println("ZoneOffset: " + zoneOffset);

        OffsetDateTime offSetByTwo = OffsetDateTime.of(localDateTime, zoneOffset);
        System.out.println("OffSetByTwo: " + offSetByTwo);
    }

    private static void period() {
        LocalDate initialDate = LocalDate.parse("2017-09-30");
        System.out.println("PeriodInitialDate: " + initialDate);

        LocalDate finalDate = initialDate.plus(Period.ofDays(5));
        System.out.println("PeriodFinalDate: " + finalDate);

        int periodDays1 = Period.between(finalDate, initialDate).getDays();
        System.out.println("PeriodDays 1: " + periodDays1);

        long periodDays2 = ChronoUnit.DAYS.between(initialDate , initialDate);
        System.out.println("PeriodDays 2: " + periodDays2);
    }

    private static void duration() {
        LocalTime initialTime = LocalTime.of(6, 30, 0);
        System.out.println("DurationInitialTime: " + initialTime);

        LocalTime finalTime = initialTime.plus(Duration.ofSeconds(30));
        System.out.println("DurationFinalTime: " + finalTime);

        long durationSeconds1 = Duration.between(finalTime, initialTime).getSeconds();
        System.out.println("DurationSeconds 1: " + durationSeconds1);

        long durationSeconds2 = ChronoUnit.SECONDS.between(finalTime, initialTime);
        System.out.println("DurationSeconds 2: " + durationSeconds2);
    }
}
