package pl.sda.annotations;

@Info(
    author = "mrzepinski",
    date = "30.09.2017",
    desc = "Class for School object"
)
public class School {
    private final String name;

    public School(String name) {
        this.name = name;
    }

    @Info(
        author = "mrzepinski",
        date = "30.09.2017",
        desc = "Get School name"
    )
    public String getName() {
        return name;
    }
}
